@ECHO off
REM Script to fairly efficiently rerun a failed upload based on the log file.
REM Usage: drop the log file on the top of this script. A new log file will be created

REM Author: Abel Szkalisity
REM abel.szkalisity@helsinki.fi
REM Ikonen group Department of Anatomy
REM Faculty of Medicine, University of Helsinki
REM Helsinki, Finland.

set logFileDrive=%~d1
REM keep only the letter
set logFileDrive=%logFileDrive:~0,1%
set logFilePath=%~p1
set oldLogFileName=%~n1

REM Extracting IDA-root and path within. This is needed to properly specify the log file location within the wsl

set pathOnIDA=
set foundRoot=false

set IDAPathOnWindows=%logFileDrive%:

CALL :LoCase logFileDrive
set IDAPathOnWSL=/mnt/%logFileDrive%

SETLOCAL EnableDelayedExpansion

set t=%logFilePath%
:FORLOOP
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
	set t=%%B
	if !foundRoot!==true (
	 	set pathOnIDA=!pathOnIDA!/%%A
	) else (
		set IDAPathOnWSL=!IDAPathOnWSL!/%%A
		set IDAPathOnWindows=!IDAPathOnWindows!\%%A
	)
	rem if the item exist
	if "%%A" == "IDA_root" set foundRoot=true
	
	if NOT "%t%"=="" goto FORLOOP
)

REM set time and date properly
set hour=%TIME:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%
set min=%TIME:~3,2%
if "%min:~0,1%" == " " set min=0%min:~1,1%
set secs=%TIME:~6,2%
if "%secs:~0,1%" == " " set secs=0%secs:~1,1%

set newLogFile=log_upload_%DATE:~-4%-%DATE:~-7,2%-%DATE:~-10,2%_%hour%-%min%-%secs%_recovery_%oldLogFileName%
set tmpIgnoreFileName=.tmpIgnore

set tmpIgnoreFileOnWin=!IDAPathOnWindows!\_SourceCode\%tmpIgnoreFileName%
set tmpIgnoreFileOnWSL=!IDAPathOnWSL!/_SourceCode/%tmpIgnoreFileName%

set logFileOnWin=%~d1%~p1\%newLogFile%
set logFileOnWSL=!IDAPathOnWSL!!pathOnIDA!/%newLogFile%

@echo THIS IS A RECOVERY LOG FILE FOR %oldLogFileName%.>%logFileOnWin%
@echo Listing here the old log file lines:>>%logFileOnWin%
@echo. >>%logFileOnWin%

REM Give some info for the user:

echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo.
echo Trying to recover failed upload.
echo The name of the log file for this process is: %logFileOnWin%
echo.
echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo.
echo Creating ignore file for this upload (this may take a while)...

type nul > %tmpIgnoreFileOnWin%

set uploadedFile=""

REM Go through on old file line-by-line, and writing out retardedly
for /F "tokens=*" %%A in (%1) do (	
	set currLine=%%A
	
	REM This is supposed to be here for nested recovery files
	if "!currLine!"=="THE UPLOAD WAS INTERRUPTED HERE" (
		set uploadedFile=""
	)
	
	@echo !currLine!>>%logFileOnWin%
	
	if NOT !uploadedFile!=="" (
		@echo !uploadedFile!>>%tmpIgnoreFileOnWin%
	)
	
	if "!currLine:~0,11!"=="Uploading: " (
		set srcOnWLS=!currLine:~11!
		set uploadSuccess=false
	)
	if "!currLine:~0,31!"=="to the Ikonen Image Datastore: " (
		set targetOnIDA=!currLine:~31!
	)
	
	if "!currLine:~0,16!"=="Uploading file: " (
		set uploadedFile=!currLine:~16!
		CALL :strlen idaPathLength targetOnIDA
		set /a "idaPathLength-=1"
		CALL :trimStart uploadedFile idaPathLength
		set uploadedFile=!srcOnWLS!!uploadedFile!
	)
	
	if "%%A"=="Target uploaded successfully." (
		set uploadSuccess=true
		type nul > %tmpIgnoreFileOnWin%
		set uploadedFile=""
	)
)

echo Creating temporary ignore file is DONE.
echo.
echo Continue uploading:
echo.

if %uploadSuccess%==false (
	@echo THE UPLOAD WAS INTERRUPTED HERE>>%logFileOnWin%
	@echo Trying again: >>%logFileOnWin%
	@echo.>>%logFileOnWin%
	
	wsl cd %IDAPathOnWSL%/_SourceCode/Unix; ./uploadToIDA-recovery "%srcOnWLS%" "%targetOnIDA%" "%logFileOnWSL%" "%tmpIgnoreFileOnWSL%"
)

echo.
echo ********************************  ---  ********************************
echo.
echo                       UPLOADING OF THE FILES HAS BEEN
echo                           SUCCESSFULLY FINISHED
echo.
echo ********************************  ---  ********************************
echo.

:exit

pause


REM **********************************************
REM ----------------------------------------------
REM **********************************************
REM Util functions

:LoCase
:: Subroutine to convert a variable VALUE to all lower case.
:: The argument for this subroutine is the variable NAME.
FOR %%i IN ("A=a" "B=b" "C=c" "D=d" "E=e" "F=f" "G=g" "H=h" "I=i" "J=j" "K=k" "L=l" "M=m" "N=n" "O=o" "P=p" "Q=q" "R=r" "S=s" "T=t" "U=u" "V=v" "W=w" "X=x" "Y=y" "Z=z") DO CALL SET "%1=%%%1:%%~i%%"
GOTO:EOF

:strlen <resultVar> <stringVar>
(   
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        set "len=1"
        for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
            if "!tmp:~%%P,1!" NEQ "" ( 
                set /a "len+=%%P"
                set "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        set len=0
    )
)
( 
    endlocal
    set "%~1=%len%"
    exit /b
)

:trimStart <stringToTrim> <nofChars>
(
setlocal EnableDelayedExpansion
set idaPathLength=!%~2!
set uploadedFile=!%~1!
:myinnerloop
if NOT "!idaPathLength!"=="0" (	
	set /a "idaPathLength-=1"
	set uploadedFile=!uploadedFile:~1!
	GOTO myinnerloop
)
)
( 
    endlocal
    set "%~1=%uploadedFile%"
    exit /b
)