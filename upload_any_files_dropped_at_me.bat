@ECHO off
REM Script to encapsulate the multiple file uploads

REM Author: Abel Szkalisity
REM abel.szkalisity@helsinki.fi
REM Ikonen group Department of Anatomy
REM Faculty of Medicine, University of Helsinki
REM Helsinki, Finland.

echo ********************************  ---  ********************************
echo                           ...    .....    .....
echo                          ...... ...... . .......
echo                       .............................
echo                        ........ IDA CLOUD ..........
echo                          ........................
echo                            ...    ...       ....
echo.
echo                                    _
echo                                   / \
echo                                  /   \
echo                                 /     \
echo                                /   _   \
echo                               /   /^|\   \
echo                              /   / ^| \   \
echo                             /___/  ^|  \___\
echo                                    ^|
echo                                    ^|
echo                                    ^|
echo                                    ^|
echo                           ___________________
echo                          ^|                   ^|
echo                          ^|    HARD DRIVE     ^|
echo                          ^|___________________^|
echo.
echo                     WELCOME TO UPLOAD TO IDA SCRIPTS
echo.
echo ********************************  ---  ********************************

TIMEOUT 7

REM ----------------------------------------------
REM Go through one-by-one on all parameters to this file, first only to notify the user
setlocal enabledelayedexpansion

set argCount=0
for %%x in (%*) do (
	set /A argCount+=1
)

REM Creating log file name from date
set hour=%TIME:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%
set min=%TIME:~3,2%
if "%min:~0,1%" == " " set min=0%min:~1,1%
set secs=%TIME:~6,2%
if "%secs:~0,1%" == " " set secs=0%secs:~1,1%
set logFileName=log_upload_%DATE:~-4%-%DATE:~-7,2%-%DATE:~-10,2%_%hour%-%min%-%secs%

REM ----------------------------------------------
REM Identifying source file location (which will identify the IDA upload space).

REM ************* HANDLING TARGET LOCATION ****************

REM The first parameter is the full path of this file which defines the location within IDA 
REM (remote path, to where we upload)
set origTgt=%0
set origTgt=%origTgt:~1,-1%
set pathOnIDA=
set foundRoot=false

REM Lowercasing the drive letter for wsl (windows subsystem for linux)
set t=%origTgt%
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
    set t=%%B
	set driveLetter=%%A
)

set driveLetter=%driveLetter:~0,1%
REM On windows it can be big
set IDAPathOnWindows=%driveLetter%:

CALL :LoCase driveLetter

set IDAPathOnWSL=/mnt/%driveLetter%

REM This for-loop's purpose is to find the folder in the remote
REM path (IDA path) which identifies the root folder of IDA. This must always be IDA_root
REM Details: The for loop goes one-by-one on the folder structure (going deeper each time).
REM The part before finding IDA_root goes to the IDAPathOnWindows (and IDAPathOnWSL) variable (+ the leading mount location) whilst
REM the remaining part is going to be the pathOnIDA, indicating the target folder within the service.

REM this is needed to work within the for loop if
SETLOCAL EnableDelayedExpansion

:FORLOOP2
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
	set t=%%B
	if !foundRoot!==true (
	 	set pathOnIDA=!pathOnIDA!/%%A
	) else (
		set IDAPathOnWSL=!IDAPathOnWSL!/%%A
		set IDAPathOnWindows=!IDAPathOnWindows!\%%A
	)
	rem if the item exist
	if "%%A" == "IDA_root" set foundRoot=true
	
	if NOT "%t%"=="" goto FORLOOP2
)

REM trim off the filename
set ownFileName=%~nx0
CALL :strlen ownLength ownFileName
set pathOnIDA=!pathOnIDA:~0,-%ownLength%!


REM ----------------------------------------------

echo.
echo.
echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo.
if %argCount%==1 (
	echo You have submitted %argCount% file to upload.
) else (
	echo You have submitted %argCount% files to upload. They will be processed one-by-one.
)
echo.
echo The name of the LOG FILE IS: %logFileName%
echo This is located under %IDAPathOnWindows%\_LogFiles
echo.
echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo.
echo Please note that at most __255__ files/folders can be submitted to this script at a time.
echo.
echo This restricts only the number of files submitted directly, e.g. you can submit 1 single folder
echo that contains thousands of files in it, it still only counts as 1.
echo.
echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo.
echo      Your upload will begin shortly...

TIMEOUT 15

echo.
echo.

for %%x in (%*) do (
	REM Uniformly put always value on current parameter hence %%~x
	call "%IDAPathOnWindows%\_SourceCode\IDA_single_upload.bat" "%%~x" "%IDAPathOnWSL%" "%pathOnIDA%" %logFileName%
)

echo.
echo ********************************  ---  ********************************
echo.
echo                       UPLOADING OF THE FILES HAS BEEN
echo                           SUCCESSFULLY FINISHED
echo.
echo ********************************  ---  ********************************
echo.

:exit

pause


REM **********************************************
REM ----------------------------------------------
REM **********************************************
REM Util functions

:LoCase
:: Subroutine to convert a variable VALUE to all lower case.
:: The argument for this subroutine is the variable NAME.
FOR %%i IN ("A=a" "B=b" "C=c" "D=d" "E=e" "F=f" "G=g" "H=h" "I=i" "J=j" "K=k" "L=l" "M=m" "N=n" "O=o" "P=p" "Q=q" "R=r" "S=s" "T=t" "U=u" "V=v" "W=w" "X=x" "Y=y" "Z=z") DO CALL SET "%1=%%%1:%%~i%%"
GOTO:EOF

REM ********* function *****************************
:strlen <resultVar> <stringVar>
(   
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        set "len=1"
        for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
            if "!tmp:~%%P,1!" NEQ "" ( 
                set /a "len+=%%P"
                set "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        set len=0
    )
)
( 
    endlocal
    set "%~1=%len%"
    exit /b
)