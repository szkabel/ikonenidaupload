How to create a new installation of this upload process?

This was done for University of Helsinki Centrally Administered machines.

1) Clone this git-hub repository into a local folder, e.g. C:\Users\Public\Documents\IDA_root ( git clone --recurse-submodules https://szkabel@bitbucket.org/szkabel/ikonenidaupload.git ). Rename the created ikonenidaupload folder to _SourceCode.

Detailed info for beginners:
	a) For this you need an installed version of git. It can be installed from the Software Center of the University currently Git 2.12.2.2
	b) Open git cmd (e.g. by typing in to start menu)
	c) In the command line type in first the drive letter of the designated target folder (in this example "C:")
	d) Change your directory to the target one with cd command (possibly also create the directory with mkdir)
		mkdir C:\Users\Public\Documents\IDA_root
		cd C:\Users\Public\Documents\IDA_root
	e) Copy the clone command git clone -- ... see above.

2) You need to acquire local administrator rights to turn on the Windows subsystem for linux feature. This is required for the IDA command line tools.

See how to apply for local admin rights here (it takes 1-2 weeks to get these rights): https://helpdesk.it.helsinki.fi/en/instructions/computer-and-printing/workstation-administrator-rights

3) Once you have the admin account log in with it and turn on the Windows Subsystem for Linux feature. This is under: ControlPanel -> Programs -> Turn Windows features on/off (this requires the admin rights) -> tick in Windows Subsystem for Linux

4) Go to Microsoft Store (e.g. start menu) and search for an Ubuntu version (at the time being 18.04 LTS is a good choice). Install it. Then open the app (Käynnistä) and wait for it to install. When it asks for a new unix username just close the window.

5) Open a command line (cmd in start menu). Type in wsl
This should bring you to a prompt within the linux system. You can verify if under /mnt you see all your drives (only physical not yet network storage such as L or P).

NOTE: Steps 6-10 can be performed with a single bash script. The file is located in the bottom of this readme. You have to change the log-in credentials there for your own IDA access.
Note that also in case of using the ./setupEnvironmentScript you should convert the script itself to dos2unix format if you edited it under windows. This can be done by running the first 2 lines of the script in a bare command line and then calling dos2unix setupEnvironmentScript

6) Type in sudo apt update (Allow the server connection with F-secure)

7) TYPE in sudo apt install dos2unix (again allow the server connection with F-secure)

8) Convert the ida upload files to unix format instead of dos:

cd /mnt/c/Users/Public/Documents/IDA_root/_SourceCode/Unix/ida2-command-line-tools
dos2unix ida
cd ..
dos2unix uploadToIDA
dos2unix downloadFromIDA

9) Create network folders:

mkdir /mnt/l
mkdir /mnt/p

NOTE: in case of other network drives (e.g. Z drive), one needs to add that drive here and also under _SourceCode/Unix/uploadToIDA and uploadToIDA-recovery in the first lines: mount -t drvfs '\\ad.helsinki.fi\group' /mnt/[YOUR DRIVE LETTER];

10) Create a netrc file in your home directory according to the template below
	a) cd ~
	b) touch .netrc
	c) nano .netrc
	d) In the opening editor type in the following lines, by naturally changing the Username and password with your IDA (CSC) credentials!
		machine ida.fairdata.fi
		login MyUsername
		password MyPassword
		e) Ctrl+O -> enter (save), ctrl+X
		
	WARNING: This will be visible to anyone checking this computer! In this case use your IDA password only for this purpose!!!
	
	NOTE: If you don't feel like sharing your password, the system can still be used, but each time the users need to type in their credentials to the command line when requested.

11) Exit the terminal (close the window)

12) Create the IDA server folder structure locally within the C:\Users\Public\Documents\IDA_root folder. 
Additionally create a folder named _LogFiles also under IDA_root (i.e. _SourceCode and _LogFiles are on the same level)

12 a) Create _Upload, _Download as well, and place there the corresponding upload_any_files_dropped_at_me and download_to_folder_dropped_at_me.

12 b) Create a shortcut on desktop for All users for this IDA_root. Note: this can only be done in command line (explorer prohibits running in elevated mode). The commands to see the desktop is dir /a.
Create a shortcut somewhere else and then in admin cmd (start menu, type in cmd, right click, run as administrator) use the move command to move the shortcut to C:\Users\Public\Desktop (the command is move src tgt)

13) Copy the "upload_any_files_dropped_at_me.bat" file to the required target folder (create several target folders for different users under IDA_root).

14) READY, drag-and-drop your files/folders on the batch file.

APPENDIX 1: setupEnvironmentScript

#!/bin/bash

sudo apt update
sudo apt install dos2unix

# Change dir to command line tools
cd /mnt/c/Users/Public/Documents/IDA_root/_SourceCode/Unix/ida2-command-line-tools
#Unixing matching files
dos2unix ida
cd ..
dos2unix uploadToIDA
dos2unix downloadFromIDA
dos2unix ida-recovery
dos2unix ida-config
dos2unix uploadToIDA-recovery

#Making directories for network attached drives (and others as well)
driveList="e f g h i j k l m n o p q r s t u v w z"

for currDrive in $driveList; do
  if [ ! -d "/mnt/${currDrive}" ]; then
    mkdir "/mnt/${currDrive}"
  fi
done

touch ~/.netrc
echo "machine ida.fairdata.fi" > ~/.netrc
echo "login szkalisi" >> ~/.netrc
echo "password MYSECRETPASSWORD" >> ~/.netrc