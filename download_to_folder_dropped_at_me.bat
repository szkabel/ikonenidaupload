@ECHO off
REM Script to enable drag-and-drop of a folder and download the data there from IDA.
REM The location of this file identifies what to download, whilst the dropped location will be the target location.
REM
REM PARAMETERS:
REM 	%1	The folder, the target of the download (destination folder)

REM Author: Abel Szkalisity
REM abel.szkalisity@helsinki.fi
REM Ikonen group Department of Anatomy
REM Faculty of Medicine, University of Helsinki
REM Helsinki, Finland.

REM NOTE: Throughout this script source is the actual destination and target is the source (as this script was copied from upload one)

echo ********************************  ---  ********************************
echo                           ...    .....    .....
echo                          ...... ...... . .......
echo                       .............................
echo                        ........ IDA CLOUD ..........
echo                          ........................
echo                            ...    ...       ....
echo.
echo                                    ^|
echo                                    ^|
echo                                    ^|
echo                                    ^|
echo                              ____  ^|  ____
echo                              \   \ ^| /   /
echo                               \   \_/   /
echo                                \       /
echo                                 \     /
echo                                  \   /
echo                                   \_/
echo                           ___________________
echo                          ^|                   ^|
echo                          ^|    HARD DRIVE     ^|
echo                          ^|___________________^|
echo.
echo                     WELCOME TO DOWNLOAD FROM IDA SCRIPTS
echo.
echo ********************************  ---  ********************************

TIMEOUT 7


REM the second parameter is the source (that was dragged upon this file)
set origSrc="%~1"

REM Get out our own filename
set ownFileName=%~nx0

REM ************* HANDLING SOURCE FILE ****************

set t=%origSrc%
REM The drive letter needs to be lower case for ubuntu
REM This is not a real loop but rather a split among the delims and then separating based on order 
REM A is 1st B is 2nd etc.
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
    set t=%%B
	set driveLetterSrc=%%A
)

REM It's 1,1 because of the leading " and we only need the letter
set driveLetterSrc=%driveLetterSrc:~1,1%
CALL :LoCase driveLetterSrc

set srcPathOnUnix=/mnt/%driveLetterSrc%

set possibleFileName=
REM remove the " from the end
set t=%t:~0,-1%
REM The goto FORLOOP1 actually implements here a for loop.
REM This is to convert between \ and / (filesep in win and linux)
:FORLOOP1
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (		
	
	set t=%%B
	set srcPathOnUnix=%srcPathOnUnix%/%%A
	if NOT "%t%"=="" goto FORLOOP1
)

REM ************* HANDLING TARGET LOCATION ****************

REM The first parameter is the full path of this file which defines the location within IDA 
REM (remote path, to where we upload)
set origTgt=%0
set origTgt=%origTgt:~1,-1%
set pathOnIDA=
set foundRoot=false

REM Lowercasing the drive letter for wsl (windows subsystem for linux)
set t=%origTgt%
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
    set t=%%B
	set driveLetter=%%A
)

set driveLetter=%driveLetter:~0,1%
REM On windows it can be big
set IDAPathOnWindows=%driveLetter%:

CALL :LoCase driveLetter

set IDAPathOnWSL=/mnt/%driveLetter%

REM This for-loop's purpose is to find the folder in the remote
REM path (IDA path) which identifies the root folder of IDA. This must always be IDA_root
REM Details: The for loop goes one-by-one on the folder structure (going deeper each time).
REM The part before finding IDA_root goes to the IDAPathOnWindows (and IDAPathOnWSL) variable (+ the leading mount location) whilst
REM the remaining part is going to be the pathOnIDA, indicating the target folder within the service.

REM this is needed to work within the for loop if
SETLOCAL EnableDelayedExpansion

:FORLOOP2
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
	set t=%%B
	if NOT %%A==%ownFileName% (
		set possibleFileName=%%A
	)
	if !foundRoot!==true (
	 	set pathOnIDA=!pathOnIDA!/%%A
	) else (
		set IDAPathOnWSL=!IDAPathOnWSL!/%%A
		set IDAPathOnWindows=!IDAPathOnWindows!\%%A
	)
	rem if the item exist
	if "%%A" == "IDA_root" set foundRoot=true
	
	if NOT "%t%"=="" goto FORLOOP2
)

REM trim off the filename
CALL :strlen ownLength ownFileName
set pathOnIDA=!pathOnIDA:~0,-%ownLength%!

REM echo %IDAPathOnWindows%
REM echo %pathOnIDA%
REM echo %possibleFileName%
REM pause
REM goto exit

	echo You have requested the following folder for download:
	echo.
	echo %pathOnIDA%	

	wsl cd %IDAPathOnWSL%/_SourceCode/Unix; ./downloadFromIDA "%srcPathOnUnix%/%possibleFileName%.zip" "%pathOnIDA%"

)

echo.
echo ********************************  ---  ********************************
echo.
echo                       DOWNLOADING OF THE FILES HAS BEEN
echo                           SUCCESSFULLY FINISHED
echo.
echo ********************************  ---  ********************************
echo.



ENDLOCAL

:exit

pause

REM **************************************
REM *********** UTIL FUNCTIONS ***********
REM **************************************

:LoCase
:: Subroutine to convert a variable VALUE to all lower case.
:: The argument for this subroutine is the variable NAME.
FOR %%i IN ("A=a" "B=b" "C=c" "D=d" "E=e" "F=f" "G=g" "H=h" "I=i" "J=j" "K=k" "L=l" "M=m" "N=n" "O=o" "P=p" "Q=q" "R=r" "S=s" "T=t" "U=u" "V=v" "W=w" "X=x" "Y=y" "Z=z") DO CALL SET "%1=%%%1:%%~i%%"
GOTO:EOF

REM ********* function *****************************
:strlen <resultVar> <stringVar>
(   
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        set "len=1"
        for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
            if "!tmp:~%%P,1!" NEQ "" ( 
                set /a "len+=%%P"
                set "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        set len=0
    )
)
( 
    endlocal
    set "%~1=%len%"
    exit /b
)