#!/bin/bash

# Notes: this file was created to double check for some files that were found missing in the first round
# It turned out that some special characters e.g. § are counted as 2 characters hence an extra 1 needs to be added
# to compensate for that (this was encodeed in the $specialLength variable). Check also the checkSHA.sh's $specialLength variable.

srcDir="/mnt/l/anat-data1/ikonengroup-images/Veijo/Nikon fixed cells imaging/"
tgtDir="/mnt/l/ltdk_ikonen/Veijo/Nikon fixed cells imaging/"
logFile="/mnt/c/Users/Public/Documents/IDA_root/_LogFiles/200703_VeijoCheckLog.txt"
#srcDir="/mnt/d/DATA/szkabel/TestCopyFolder/"
#tgtDir="/mnt/l/ltdk_ikonen/Abel/TestCopyFolder/"

echo
echo

srcPrefixLength=`expr ${#srcDir} + 1`

while read p; do   
  header=$(echo "$p" | cut -c1-14)
  if [ "$header" == "MISSING FILE: " ]; then	      
  
	specialLength=`expr ${#p} + 1`
	oldLine=$(echo "$p" | cut -c15-$specialLength)
	myRes=$(./checkSHA.sh "$oldLine" "$tgtDir" $srcPrefixLength)	
	if [ "$myRes" != "OK" ]; then	
		echo "$myRes"
	fi
  else
    if [ "$header" == "WRONG SHA for:" ]; then
		echo $p
	fi
  fi
done < $logFile

echo
echo "If this is the single output line, then all the files were copied correctly. Otherwise check the log."
echo
