#!/bin/bash

srcSHA=$(/mnt/c/Users/Public/Documents/IDA_root/_SourceCode/Unix/ida2-command-line-tools/ida-checksum "$1")

specialLength=`expr ${#1} + 1`
currentPostfix=$(echo "$1" | cut -c $3-$specialLength)

currentTgtFile="$2$currentPostfix"

#echo "$currentTgtFile"
#echo "$myFileName"

#if [ "$currentTgtFile" == "$myFileName" ]; then
#	echo "The 2 string are equal"
#else
#	echo "Sg is WRONG"
#fi

#echo "DIFF:"
#diff  <(echo "$currentTgtFile" ) <(echo "$myFileName")

#echo "CMP:"
#cmp -bl <(echo -n "$currentTgtFile") <(echo -n "$myFileName")

#if [ -f "$myFileName" ]; then
#	echo "The file exists!"
#else
#	echo "MISSING!"
#fi

if [ -f "$currentTgtFile" ]; then
    tgtSHA=$(/mnt/c/Users/Public/Documents/IDA_root/_SourceCode/Unix/ida2-command-line-tools/ida-checksum "$currentTgtFile")
    if [ $srcSHA == $tgtSHA ]; then
		echo "OK"
    else
	echo "WRONG SHA for: $1"
    fi

else 
    echo "MISSING FILE: $1"
fi
