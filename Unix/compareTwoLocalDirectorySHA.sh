#!/bin/bash

srcDir="/mnt/l/anat-data1/ikonengroup-images/Veijo/Nikon fixed cells imaging/"
tgtDir="/mnt/l/ltdk_ikonen/Veijo/Nikon fixed cells imaging/"
#srcDir="/mnt/d/DATA/szkabel/TestCopyFolder/"
#tgtDir="/mnt/l/ltdk_ikonen/Abel/TestCopyFolder/"

echo
echo

srcPrefixLength=`expr ${#srcDir} + 1`

find "$srcDir" -type f |
while read p; do
  myRes=$(./checkSHA.sh "$p" "$tgtDir" $srcPrefixLength)
  if [ "$myRes" != "OK" ]; then	
	echo "$myRes"
  fi
done

echo
echo "If this is the single output line, then all the files were copied correctly. Otherwise check the log."
echo
