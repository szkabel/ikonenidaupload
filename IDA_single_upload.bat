@ECHO off
REM Script to enable drag-and-drop of a single entity (file or folder) upload to the IDA service.
REM
REM PARAMETERS:
REM 	%1	The file to be uploaded with its full path on windows
REM 	%2	The path of the IDA_root in the windows subsystem for linux distro (i.e. /mnt/driveLetter/<pathOnWindows with /-s>)
REM 	%3	The path within IDA_root for the file to be uploaded
REM 	%4	The filename (not the path) for the log file

REM Author: Abel Szkalisity
REM abel.szkalisity@helsinki.fi
REM Ikonen group Department of Anatomy
REM Faculty of Medicine, University of Helsinki
REM Helsinki, Finland.

REM the second parameter is the source (that was dragged upon this file)
set origSrc=%1

REM ************* HANDLING SOURCE FILE ****************

set t=%origSrc%
REM The drive letter needs to be lower case for ubuntu
REM This is not a real loop but rather a split among the delims and then separating based on order 
REM A is 1st B is 2nd etc.
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (
    set t=%%B
	set driveLetterSrc=%%A
)

REM It's 1,1 because of the leading " and we only need the letter
set driveLetterSrc=%driveLetterSrc:~1,1%
CALL :LoCase driveLetterSrc

set srcPathOnUnix=/mnt/%driveLetterSrc%

set possibleFileName=
REM remove the " from the end
set t=%t:~0,-1%
REM The goto FORLOOP1 actually implements here a for loop.
REM This is to convert between \ and / (filesep in win and linux)
:FORLOOP1
For /F "tokens=1* delims=\" %%A IN ("%t%") DO (		
	set possibleFileName=%%A
	set t=%%B
	set srcPathOnUnix=%srcPathOnUnix%/%%A
	if NOT "%t%"=="" goto FORLOOP1
)

REM set possibleFileName=%possibleFileName:~0,-1%

REM ************* HANDLING TARGET FILE ****************
REM This was done outside

set IDAPathOnWSL=%2
set pathOnIDA=%3

REM remove the leading and trailing "-s from the parameters
set IDAPathOnWSL=%IDAPathOnWSL:~1,-1%
set pathOnIDA=%pathOnIDA:~1,-1%

REM echo %IDAPathOnWSL%
REM echo %pathOnIDA%
REM echo %possibleFileName%
REM pause
REM goto exit

echo.
echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo /**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\
echo.

REM Check if this is folder/file

set fileType=
For %%Z In (%origSrc%) Do (
	If "%%~aZ" GEq "d" (
		set fileType=DIRECTORY
	) Else (
		If "%%~aZ" GEq "-" (
			set fileType=FILE
		) Else (
			set fileType=inaccessible
		)
	)
)

If NOT %fileType% == inaccessible (

	echo You have submitted the following %fileType% for upload:
	echo.
	echo %origSrc%

	wsl cd %IDAPathOnWSL%/_SourceCode/Unix; ./uploadToIDA "%srcPathOnUnix%" "%pathOnIDA%%possibleFileName%" %4

)


echo.
echo /**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\
echo *\/**\/**\/**\/**\/**\/**\/**\/**\/**\/**\/*
echo.

ENDLOCAL

:exit

REM **************************************
REM *********** UTIL FUNCTIONS ***********
REM **************************************

:LoCase
:: Subroutine to convert a variable VALUE to all lower case.
:: The argument for this subroutine is the variable NAME.
FOR %%i IN ("A=a" "B=b" "C=c" "D=d" "E=e" "F=f" "G=g" "H=h" "I=i" "J=j" "K=k" "L=l" "M=m" "N=n" "O=o" "P=p" "Q=q" "R=r" "S=s" "T=t" "U=u" "V=v" "W=w" "X=x" "Y=y" "Z=z") DO CALL SET "%1=%%%1:%%~i%%"
GOTO:EOF
